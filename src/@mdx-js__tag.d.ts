declare module '@mdx-js/tag' {
  interface Props {
    components: { [key: string]: React.ComponentType }
  }
  export class MDXProvider extends React.Component<Props> {}
}
