const slidesModules = [
  import('./Title.mdx'),
  import('./Session1.mdx'),
  import('./TypescriptSummary.mdx'),
  import('./TypescriptSummaryGoodies.mdx'),
  import('./TypescriptSummaryGoodies2.mdx'),
  import('./CodeExample.mdx'),
  import('./CodeExampleCommented.mdx'),
  import('./TypesystemPrimitives.mdx'),
  import('./TypesystemPrimitivesInferred.mdx'),
  import('./TypesystemUndefinedAndNull.mdx'),
  import('./TypesystemAny.mdx'),
  import('./TypesystemAnyCaveats.mdx'),
  import('./TypesystemUnknown.mdx'),
  import('./TypesystemFunctionTypesBasics.mdx'),
  import('./TypesystemFunctionTypesInterface.mdx'),
  import('./TypesystemFunctionTypesReturningFunctions.mdx'),
  import('./TypesystemObjectTypesInterface.mdx'),
  import('./TypesystemObjectTypesInterfaceExample.mdx'),
  import('./TypesystemObjectTypesInterfaceOptionals.mdx'),
  import('./TypesystemObjectTypesInterfaceOptionalsGuarded.mdx'),
  import('./TypesystemObjectTypesInterfaceFunctions.mdx'),
  import('./TypesystemObjectTypesInterfaceFunctionsImplemented.mdx'),
  import('./TypesystemExternalValidationProblem.mdx'),
  import('./TypesystemExternalValidationIsType.mdx'),
]

import { allModules } from 'slides/allModules'
export const slides = allModules(slidesModules)
