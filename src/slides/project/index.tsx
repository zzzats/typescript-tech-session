import { allModules } from 'slides/allModules'

const slidesModules = [
  import('./Title.mdx'),
  import('./Session3.mdx'),
  import('./CompilerConfig.mdx'),
  import('./Linter.mdx'),
  import('./TypeDefinitions.mdx'),
  import('./TypeDefinitionsCustom.mdx'),
  import('./TestSetup.mdx'),
]

export const slides = allModules(slidesModules)
