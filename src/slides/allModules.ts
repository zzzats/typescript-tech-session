export interface Module<T> {
  default: T
}

export function allModules<T>(promises: Promise<Module<T>>[]): Promise<T[]> {
  return Promise.all(promises).then(slides => slides.map(slide => slide.default))
}
