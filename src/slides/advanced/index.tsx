const slidesModules = [
  import('./Title.mdx'),
  import('./Session2.mdx'),
  import('./ReadonlyModifier.mdx'),
  import('./TypeAlias.mdx'),
  import('./TypeOf.mdx'),
  import('./IndexedTypes.mdx'),
  import('./LiteralTypes.mdx'),
  import('./Overloading.mdx'),
  import('./Generics.mdx'),
  import('./GenericsConstraints.mdx'),
  import('./CombinationTypes.mdx'),
  import('./IntersectionVenn.mdx'),
  import('./IntersectionTypes.mdx'),
  import('./UnionVenn.mdx'),
  import('./UnionTypes.mdx'),
  import('./UnionTypesKeyOf.mdx'),
  import('./UnionTypesDiscriminated.mdx'),
  import('./MappedTypesBasics.mdx'),
  import('./MappedTypesSetState.mdx'),
  import('./Questions.mdx'),
]

import { allModules } from 'slides/allModules'
export const slides = allModules(slidesModules)
