export interface Mdx {
  (props?: any): JSX.Element
}
