import React from 'react'
import MonacoEditor from 'react-monaco-editor'
import { withRoute, InjectedRoute } from 'react-router5'
import { SlideNumberConsumer } from 'common/slide-number'
import * as monacoEditor from 'monaco-editor'

interface Props extends InjectedRoute {
  className?: string
  children?: any
}

interface State {
  renderEditor: boolean
  code: string
}

const EDITOR_HEIGHT = 300
const EDITOR_WIDTH = '100%'
const EDITOR_FONT_SIZE = 22

class Code extends React.Component<Props, State> {
  // HAX
  static firstRender = true
  static getDerivedStateFromProps(): Partial<State> {
    const renderEditor = Code.firstRender
    Code.firstRender = false
    return {
      renderEditor,
    }
  }

  state: State = {
    renderEditor: true,
    code: this.props.children || '',
  }

  onChange = (code: string) => {
    this.setState({ code })
  }

  // HAX
  componentDidUpdate() {
    if (!this.state.renderEditor) {
      setTimeout(() => this.setState({ renderEditor: true }), 10)
    }
  }

  editorDidMount =
    (_editor: monacoEditor.editor.IStandaloneCodeEditor, monaco: typeof monacoEditor) => {
      monaco.languages.typescript.typescriptDefaults.setCompilerOptions({
        strict: true,

        // Default options, don't remove these or editor stops intellisense
        allowNonTsExtensions: true,
        target: 5,
      })
    }

  render() {
    const currentSlide = parseInt(this.props.route!.params.slide, 10)
    const shouldRender = (slideNumber: number) => {
      console.log(this.state.renderEditor)
      return this.state.renderEditor && slideNumber === currentSlide
    }
    return (
      <SlideNumberConsumer>
        {slideNumber =>
          !shouldRender(slideNumber) ? (
            <div style={{ height: EDITOR_HEIGHT, width: EDITOR_WIDTH }} />
          ) : (
            <MonacoEditor
              language="typescript"
              height={EDITOR_HEIGHT}
              width={EDITOR_WIDTH}
              theme="vs-dark"
              value={this.state.code}
              onChange={this.onChange}
              editorDidMount={this.editorDidMount}
              options={{
                minimap: { enabled: false },
                fontSize: EDITOR_FONT_SIZE,
              }}
            />
          )
        }
      </SlideNumberConsumer>
    )
  }
}

export default withRoute(Code)
