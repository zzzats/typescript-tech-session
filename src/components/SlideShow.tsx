import React from 'react'
import styled from 'styled-components'
import { withRoute, InjectedRoute } from 'react-router5'
import WhiteLogo from './logo_white.svg'
import { SlideNumberProvider } from 'common/slide-number'
import { SlideRouteName } from 'router/router'

const Viewport = styled.div`
  width: 100%;
  height: 100%;
  overflow-x: hidden;
`

interface SlidesContainerProps {
  items: number
  slide: number
  children: JSX.Element[]
}

const SlidesContainer = styled<SlidesContainerProps, 'div'>('div')`
  width: ${p => p.children.length * 100}vw;
  height: 100%;
  overflow-x: hidden;
  display: flex;
  transition: transform 0.3s linear;
  transform: translate(${p => (p.slide - 1) * -100}vw, 0);
`

const Slide = styled.div`
  width: 100vw;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  overflow: hidden;

  > div {
    width: 80vw;
    height: 100vh;
    padding: 2.5vh 5vw;
  }

  h1 {
    color: white;
  }

  p {
    margin: 0
  }

  em {
    color: yellow;
  }

  h1 > code, h2 > code, h3 > code, h4 > code, li > code, p > code {
    color: #b6c857;
    font-weight: 600;
  }
`

const Previous = styled.div`
  position: absolute;
  display: float;
  top: 0;
  left: 0;
  width: 10vw;
  height: 100vh;
  background-color: rgba(0, 0, 0, 0);
  cursor: pointer;

  transition: background-color 0.3s linear;

  &:hover {
    background-color: rgba(0, 0, 0, 0.3);
  }
`

const Next = Previous.extend`
  left: 90vw;
`

const Logo = styled.img`
  position: absolute;
  top: 3rem;
  right: 4rem;
  height: 2rem;
`

interface Props extends InjectedRoute {
  routeName: SlideRouteName
  slides: Promise<React.ComponentType[]>
  slide: number
}

interface State {
  loaded: boolean
  slideElements: JSX.Element[]
}

class SlideShow extends React.Component<Props, State> {
  state: State = {
    loaded: false,
    slideElements: [],
  }

  componentDidMount() {
    this.props.slides
      .then(components =>
        components.map((Contents, i) => (
          <Slide key={i}>
            <SlideNumberProvider value={i + 1}>
              <Contents />
            </SlideNumberProvider>
          </Slide>
        )),
      )
      .then(slideElements => this.setState({ slideElements, loaded: true }))
      .catch(error => { console.error(error) })
    document.addEventListener('keydown', this.keyDown)
  }

  previous = () => {
    const { slide, router, routeName } = this.props
    router.navigate(routeName, { slide: Math.max(1, slide - 1) })
  }

  next = () => {
    const { slide, router, routeName } = this.props
    router.navigate(routeName, {
      slide: Math.min(this.state.slideElements.length, slide + 1),
    })
  }

  keyDown = (event: KeyboardEvent) => {
    const target = event.target
    if (target && (target as Element).nodeName === 'TEXTAREA') {
      return
    }
    const LEFT = 37
    const RIGHT = 39

    if (event.which === LEFT) {
      this.previous()
    } else if (event.which === RIGHT) {
      this.next()
    }
  }

  render() {
    const { loaded, slideElements } = this.state
    if (!loaded) {
      return <p>Loading</p>
    }

    const { slide } = this.props
    return (
      <Viewport>
        <SlidesContainer items={slideElements.length} slide={slide}>
          {slideElements}
        </SlidesContainer>
        <Logo src={WhiteLogo} />
        <Previous onClick={this.previous} />
        <Next onClick={this.next} />
      </Viewport>
    )
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.keyDown)
  }
}

export default withRoute(SlideShow)
