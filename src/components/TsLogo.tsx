import React from 'react'
import TsLogo from './typescript_logo.svg'
import styled from 'styled-components'

const Logo = styled.img`
  height: 50vh;
  float: right;
  background-color: white;
`

export default () => <Logo src={TsLogo} />
