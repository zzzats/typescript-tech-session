import Diagram from './intersection.png'
import React from 'react'

const IntersectionVenn = () => <img src={Diagram} />

export default IntersectionVenn
