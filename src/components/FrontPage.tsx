import * as React from 'react'
import { Link } from 'react-router5'
export default () => {
  return (
    <div>
      <h1>TypeScript mini course</h1>
      <h3>Murtsi & Tunkki</h3>
      <hr />
      <h2>Sections</h2>
      <ul>
        <li>
          <Link routeName="basics" routeParams={{ slide: 1 }}>
            Session 1 - The basics
          </Link>
        </li>
        <li>
          <Link routeName="advanced" routeParams={{ slide: 1 }}>
            Session 2 - Advanced types
          </Link>
        </li>
        <li>
        <Link routeName="project" routeParams={{ slide: 1 }}>
          Session 3 - Real world
        </Link>
        </li>
      </ul>
    </div>
  )
}
