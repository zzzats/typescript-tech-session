import { withRoute, InjectedRoute } from 'react-router5'
import React, { SFC } from 'react'
import FrontPage from 'components/FrontPage'
import SlideShow from 'components/SlideShow'
import { slides as basics } from 'slides/basics'
import { slides as advanced } from 'slides/advanced'
import { slides as project } from 'slides/project'

import { SlideRouteName, isSlideRouteName } from 'router/router'
import { Mdx } from 'slides/Mdx'

const NotFound = () => <h1>404 Not Found</h1>

const routeToSlides: Record<SlideRouteName, Promise<Mdx[]>> = {
  basics,
  advanced,
  project,
}

const Routes: SFC<InjectedRoute> = ({ route }) => {
  if (!route) {
    return <NotFound />
  }

  if (route.name === 'lander') {
    return <FrontPage />
  }

  if (isSlideRouteName(route.name)) {
    const slides = routeToSlides[route.name]
    const slide = parseInt(route.params.slide, 10)
    return <SlideShow slides={slides} slide={slide} routeName={route.name} />
  }

  return <NotFound />
}

export default withRoute(Routes)
