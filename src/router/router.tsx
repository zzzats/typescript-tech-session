import createRouter from 'router5'
import browserPlugin from 'router5/plugins/browser'

function typedRoutes<T extends string>(routes: {name: T, path: string}[]) {
  return routes
}

const slideRoutes = typedRoutes([
  { name: 'basics', path: '/basics/:slide' },
  { name: 'advanced', path: '/advanced/:slide' },
  { name: 'project', path: '/project/:slide' },
])

const otherRoutes = [
  { name: 'lander', path: '/' },
]

const routes = [
  ...otherRoutes,
  ...slideRoutes,
]

type GetRouteNames<T> = T extends { name: infer N }[] ? N : never

export type SlideRouteName = GetRouteNames<typeof slideRoutes>

export function isSlideRouteName(value: string): value is SlideRouteName {
  return slideRoutes.map(({ name }) => name).indexOf(value as any) >= 0
}

export const router = createRouter(routes).usePlugin(browserPlugin())

router.start()
