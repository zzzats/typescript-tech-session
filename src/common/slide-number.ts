import React from 'react'

export const {
  Provider: SlideNumberProvider,
  Consumer: SlideNumberConsumer,
} = React.createContext(-1)
