import React from 'react'
import ReactDOM from 'react-dom'
import { RouterProvider } from 'react-router5'
import { router } from 'router/router'
import Routes from 'components/Routes'
import { MDXProvider } from '@mdx-js/tag'
import Code from 'components/Code'
import { injectGlobal } from 'styled-components'

const ROOT_DIV_ID = 'app'
const main: HTMLElement | null = document.getElementById(ROOT_DIV_ID)

injectGlobal`
  * {
    box-sizing: border-box;
  }

  html {
    font-size: 62.5%;
  }

  body {
    background: #1e1e1e;
    color: white;
    font-family: 'Montserrat', sans-serif;
    height: 100%;
    margin: 0;
    padding: 0;
    font-size: 2.4rem;
  }

  #app {
    display: flex;
    justify-content: center;
    height: 100%;
  }

  h1 {
    font-size: 7rem;
  }

  li {
    font-size: 3rem
    line-height: 5rem
  }
`

if (main !== null) {
  ReactDOM.render(
    <RouterProvider router={router}>
      <MDXProvider components={{ code: Code }}>
        <Routes />
      </MDXProvider>
    </RouterProvider>,
    main,
  )
} else {
  console.error('Main render failed, query for #' + ROOT_DIV_ID + ' failed')
}
