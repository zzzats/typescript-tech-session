import { Configuration } from 'webpack'
import path from 'path'
import createStyledComponentsTransformer from 'typescript-plugin-styled-components'
import MonacoWebpackPlugin from 'monaco-editor-webpack-plugin'
import history from 'connect-history-api-fallback'
import convert from 'koa-connect'

const styledComponentsTransformer = createStyledComponentsTransformer()

const config: Configuration & { serve: any } = {
  name: 'typescript-session',
  target: 'web',
  mode: 'development',
  entry: './src/index.tsx',
  output: {
    publicPath: '/static/',
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
    modules: [
      path.resolve(__dirname, './src'),
      path.resolve(__dirname, './node_modules'),
    ],
  },
  module: {
    rules: [
      {
        test: /\.mdx$/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: ['@babel/preset-env', '@babel/preset-react'],
            },
          },
          '@mdx-js/loader',
        ],
      },
      {
        test: /.css$/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        loader: 'file-loader',
      },
      {
        test: /\.tsx?$/,
        use: [
          {
            loader: 'ts-loader',
            options: {
              configFile: 'tsconfig.web.json',
              getCustomTransformers: () => ({
                before: [styledComponentsTransformer],
              }),
            },
          },
          {
            loader: 'babel-loader',
            options: {
              // All of this to get Babel make the hot reload magic transformation
              plugins: [
                '@babel/plugin-syntax-jsx',
                '@babel/plugin-syntax-typescript',
                '@babel/plugin-syntax-dynamic-import',
                'react-hot-loader/babel',
              ],
            },
          },
        ],
      },
    ],
  },
  serve: {
    devMiddleware: {
      publicPath: '/static/',
    },
    host: '0.0.0.0',
    add: (app: any) => app.use(convert(history({}))),
  },
  plugins: [
    new MonacoWebpackPlugin({
      languages: ['typescript', 'json'],
    }),
  ],
}

export default config
