# TypeScript tech session for for Wunderdog

## Development

```
yarn install
yarn start

--> http://localhost:1234/slides/1
```

## Building

```
yarn install
yarn build

--> dist/
```
